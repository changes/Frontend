package net.lo1c.changes.networking.utils;

import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.lo1c.changes.type.Building;
import net.lo1c.changes.type.Camera;
import net.lo1c.changes.type.Plate;
import net.lo1c.changes.utils.RequestURL;
import net.lo1c.changes.utils.Result;

public class Networking {

    public static Result getResult(Double leftLat, Double leftLong, Double rightLat, Double rightLong) throws JSONException {
        try {
            List<Plate> pl = new ArrayList<>();
            List<Building> bl = new ArrayList<>();
            List<Camera> cl = new ArrayList<>();

            try {
                String url = RequestURL.getURL("https://changes.plantio.de/", leftLat, leftLong, rightLat, rightLong);
                JSONArray jsonarray = getJSONObject(url).getJSONArray("answer");

                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject o = jsonarray.getJSONObject(i);
                    String type = o.getString("type");
                    if (type.equals("plate")) {
                        pl.add(new Plate(o, o.getJSONObject("data")));
                    } else if (type.equals("camera")) {
                        cl.add(new Camera(o, o.getJSONObject("data")));
                    } else if (type.equals("building")) {
                        bl.add(new Building(o, o.getJSONObject("data")));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return new Result(pl, cl, bl);
        } catch (Exception e) {
            return null;
        }
    }

    private static JSONObject getJSONObject(String url) {
        try {
            return new JSONObject(readUrl(url));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }

    }

}
