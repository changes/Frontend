package net.lo1c.changes.type;

public class KeySet {

    private Double latitude;
    private Double longitude;

    public KeySet(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

}
