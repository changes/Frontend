package net.lo1c.changes.type;

import org.json.JSONException;
import org.json.JSONObject;

import net.lo1c.changes.utils.Date;

public class Plate extends KeySet {

    private Date deportationDate;
    private String deportationDestination;
    private Date deathDate;
    private String deathPlace;
    private String name;

    public Plate(JSONObject jsonobject, JSONObject dataobject) throws JSONException {
        super(jsonobject.getDouble("lat"), jsonobject.getDouble("long"));

        this.deathDate = new Date(dataobject.getString("deathDate"));
        this.deportationDate = new Date(dataobject.getString("deportationDate"));

        this.deathPlace = dataobject.getString("deathPlace");
        this.deportationDestination = dataobject.getString("deportationDestination");

        this.name = dataobject.getString("name");
    }

    public Date getdDeathDate() {
        return deathDate;
    }

    public String getdDeathPlace() {
        return deathPlace;
    }

    public Date getdDeportationDate() {
        return deportationDate;
    }

    public String getdDeportationDestination() {
        return deportationDestination;
    }

    public String getdName() {
        return name;
    }


}
