package net.lo1c.changes.type;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.lo1c.changes.utils.Image;

public class Building extends KeySet {

    private ArrayList<Image> imageList = new ArrayList<>();

    public Building(JSONObject jsonobject, JSONObject dataobject) throws JSONException {
        super(jsonobject.getDouble("lat"), jsonobject.getDouble("long"));
        JSONArray imageArray = dataobject.getJSONArray("images");

        for (int i = 0; i < imageArray.length(); i++) {
            JSONObject obj = imageArray.getJSONObject(i);
            Image img = new Image(obj.getInt("year"), obj.getString("hash"));
            this.imageList.add(img);
        }

    }

    public ArrayList<Image> getImageList() {
        return imageList;
    }

}