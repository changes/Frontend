package net.lo1c.changes.type;

import org.json.JSONException;
import org.json.JSONObject;

public class Camera extends KeySet {

    private String street;
    private String housenumber;
    private String name;
    private String surveillance;

    public Camera(JSONObject jsonobject, JSONObject dataobject) throws JSONException {
        super(jsonobject.getDouble("lat"), jsonobject.getDouble("long"));

        this.street = dataobject.getString("street");
        this.housenumber = dataobject.getString("housenumber");
        this.name = dataobject.getString("name");
        this.surveillance = dataobject.getString("surveillance");
    }

    public String getStreet() {
        return street;
    }

    public String getHousenumber() {
        return housenumber;
    }

    public String getName() {
        return name;
    }

    public String getSurveillance() {
        return surveillance;
    }

}
