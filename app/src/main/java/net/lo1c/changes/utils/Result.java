package net.lo1c.changes.utils;

import java.util.List;

import net.lo1c.changes.type.Building;
import net.lo1c.changes.type.Camera;
import net.lo1c.changes.type.Plate;

public class Result {

    private final List<Plate> plateList;
    private final List<Camera> cameraList;
    private final List<Building> buildingList;

    public Result(List<Plate> plateList, List<Camera> cameraList, List<Building> buildingList) {
        this.plateList = plateList;
        this.cameraList = cameraList;
        this.buildingList = buildingList;
    }

    public List<Building> getBuildingList() {
        return buildingList;
    }

    public List<Camera> getCameraList() {
        return cameraList;
    }

    public List<Plate> getPlateList() {
        return plateList;
    }

}
