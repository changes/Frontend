package net.lo1c.changes.utils;

public class RequestURL {

    public static String getURL(String url, Double leftLat, Double leftLong, Double rightLat, Double rightLong) {
        StringBuilder sb = new StringBuilder();
        sb.append(url);

        if (!url.endsWith("/"))
            sb.append('/');

        sb.append("get?")
                .append("leftLat=" + leftLat + '&')
                .append("leftLong=" + leftLong + '&')
                .append("rightLat=" + rightLat + '&')
                .append("rightLong=" + rightLong);

        return sb.toString();
    }

}
