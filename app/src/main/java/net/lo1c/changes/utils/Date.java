package net.lo1c.changes.utils;

public class Date {

    private String date;

    public Date(String date) {
        if (date == "") {
            this.date = "N/A";
            return;
        }

        if (date.contains("/"))
            date = date.replace('/', '.');
        this.date = date;
    }

    public String getDate() {
        return date;
    }


}
