package net.lo1c.changes.utils;

public class Image {

    private Integer year;
    private String hash;

    public Image(Integer year, String hash) {
        this.year = year;
        this.hash = hash;
    }

    public String getHash() {
        return hash;
    }

    public Integer getYear() {
        return year;
    }

}
