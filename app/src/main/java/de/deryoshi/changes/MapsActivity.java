package de.deryoshi.changes;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import net.lo1c.changes.networking.utils.Networking;
import net.lo1c.changes.type.Building;
import net.lo1c.changes.type.Camera;
import net.lo1c.changes.type.Plate;
import net.lo1c.changes.utils.Result;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, GoogleMap.OnCameraIdleListener {

    List<Plate> plateList;
    List<Building> buildingList;
    List<Camera> cameraList;

    private BottomSheetBehavior bottomSheetBehavior;

    private Handler handler = new Handler(Looper.getMainLooper());

    private GoogleMap googleMap;

    private ArrayList<Marker> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        NestedScrollView bottomSheet = (NestedScrollView) findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
    }

    @Override
    public void onMapReady(GoogleMap gMap) {
        googleMap = gMap;
        googleMap.setMinZoomPreference(5);
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnCameraIdleListener(this);
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 15));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        TextView textView = (TextView) findViewById(R.id.text_view_title);
        TextView textView1 = (TextView) findViewById(R.id.text_view_content);
        if (marker.getTag() instanceof Plate) {
            Plate plate = (Plate) marker.getTag();
            textView.setText("Stolperstein (" + plate.getdName() + ")");
            String Deportationsdatum = !plate.getdDeportationDate().getDate().equals("") ? ("Deportationsdatum: " + plate.getdDeportationDate().getDate() + "\n") : "";
            String Deportationsrichtung = !plate.getdDeportationDestination().equals("") ? ("Deportationsrichtung: " + plate.getdDeportationDestination() + "\n") : "";
            String Todesdatum = !plate.getdDeathDate().getDate().equals("") ? ("Todesdatum: " + plate.getdDeathDate().getDate() + "\n") : "";
            String Todesort = !plate.getdDeathPlace().equals("") ? ("Todesort: " + plate.getdDeathPlace()) : "";
            if (!Deportationsdatum.equals("") || !Deportationsrichtung.equals("") || !Todesdatum.equals("") || !Todesort.equals("")) {
                textView1.setText(Deportationsdatum + Deportationsrichtung + Todesdatum + Todesort);
            } else {
                String noDatas = "- Leider keine Daten verfügbar. -";
                textView1.setText(noDatas);
            }
        } else if (marker.getTag() instanceof Building) {
            Building building = (Building) marker.getTag();
            textView.setText("Gebäude (" + building.getLatitude() + ", " + building.getLongitude() + ")");


            textView1.setText("Hier kommen noch Bilder hin! :)");


        } else if (marker.getTag() instanceof Camera) {
            Camera camera = (Camera) marker.getTag();
            textView.setText("Kamera (" + camera.getLatitude() + ", " + camera.getLongitude() + ")");
            String Kameraname = (!camera.getName().equalsIgnoreCase("undefined") && !camera.getName().equals("")) ? ("Kameraname: " + camera.getName() + "\n") : "";
            /* wird bei String Strasse benutzt */ String Hausnummer = (!camera.getHousenumber().equalsIgnoreCase("undefined") && !camera.getHousenumber().equals("")) ? camera.getHousenumber() : "";
            String Strasse = (!camera.getStreet().equalsIgnoreCase("undefined") && !camera.getStreet().equals("")) ? ("Straße: " + camera.getStreet() + " " + Hausnummer + "\n") : "";
            String Kameratyp = (!camera.getSurveillance().equalsIgnoreCase("undefined") && !camera.getSurveillance().equals("")) ? ("Kameratyp: " + (camera.getSurveillance().equalsIgnoreCase("indoor") ? "Innenkamera" : "Außenkamera")) : "";
            if (!Kameraname.equals("") || !Strasse.equals("") || !Kameratyp.equals("")) {
                textView1.setText(Kameraname + Strasse + Kameratyp);
            } else {
                String noDatas = "- Leider keine Daten verfügbar. -";
                textView1.setText(noDatas);
            }
        }

        Object obj = marker.getTag();

        if (obj instanceof Plate || obj instanceof Building || obj instanceof Camera) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
        return false;
    }

    @Override
    public void onCameraIdle() {
        for (Marker marker : list) {
            marker.remove();
        }
        final LatLngBounds llb = googleMap.getProjection().getVisibleRegion().latLngBounds;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Result r = Networking.getResult(getLeLa(llb), getLeLo(llb), getRiLa(llb), getRiLo(llb));

                    plateList = r.getPlateList();
                    buildingList = r.getBuildingList();
                    cameraList = r.getCameraList();

                    for (final Plate plate : plateList) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Marker marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(plate.getLatitude(), plate.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.plate_small)));
                                marker.setTag(plate);
                                list.add(marker);
                            }
                        });
                    }

                    for (final Building building : buildingList) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Marker marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(building.getLatitude(), building.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.building_small)));
                                marker.setTag(building);
                                list.add(marker);
                            }
                        });
                    }

                    for (final Camera camera : cameraList) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Marker marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(camera.getLatitude(), camera.getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.drawable.camera_small)));
                                marker.setTag(camera);
                                list.add(marker);
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private Double getLeLa(LatLngBounds latLngBounds) {
        return latLngBounds.southwest.latitude;
    }

    private Double getLeLo(LatLngBounds latLngBounds) {
        return latLngBounds.southwest.longitude;
    }

    private Double getRiLa(LatLngBounds latLngBounds) {
        return latLngBounds.northeast.latitude;
    }

    private Double getRiLo(LatLngBounds latLngBounds) {
        return latLngBounds.northeast.longitude;
    }
}